Vagrant setup for OpenERP
###########################

This vagrant setup provides you with a ready to be used OpenERP installation.

What's included?
-----------------

* Ubuntu LTS, 32 bit
* Postgresql
* OpenERP

How to use it?
---------------

1. Clone the repo
2. Run `vagrant up`
3. Open http://localhost:8000 in your browser

You can add additional addons under the `addons/` directory.

How does it work?
------------------

The installation is provided by puppet scripts.

1. installs some basic packages
2. installs postgresql
3. installs OpenERP by 

  * downloading the latest nightly build (.deb file)
  * installing required packaged
  * installing the debian package
  * overwriting the default config file
  * restarting the service

How to customize?
------------------

You can customize it in any way you want, but there are a few simple tweaks.

* `puppet/vagrant.pp` defines the `$OPENERP_VERSION` variable that will be downloaded and installed
* the `addons` directory can contain custom OpenERP addons
